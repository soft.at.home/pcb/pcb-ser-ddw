/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>

#include <debug/sahtrace.h>
#include <pcb/core/serialize.h>

#include "ddw.h"

static void ddw_unregister() {
    ddw_serialize_unregister();
}

void libpcb_serialize_ddw_register() {
    serialize_handlers_t* handlers = (serialize_handlers_t*) calloc(1, sizeof(serialize_handlers_t));
    if(!handlers) {
        return;
    }

    handlers->notificationBroadcastSupport = true;
    handlers->serialize_reply_end = ddw_serialize_reply_end;
    handlers->serialize_request = ddw_serialize_request;
    handlers->serialize_functionReturnValue = ddw_serialize_functionReturnValue;
    handlers->serialize_functionReturnArguments = ddw_serialize_functionReturnArguments;
    handlers->serialize_objectBegin = ddw_serialize_objectBegin;
    handlers->serialize_objectEnd = ddw_serialize_objectEnd;
    handlers->serialize_parameter = ddw_serialize_parameter;
    handlers->serialize_notification = ddw_serialize_notification;
    handlers->serialize_error = ddw_serialize_error;

    handlers->deserialize_checkFormat = ddw_deserialize_checkFormat_V01;
    handlers->deserialize = ddw_deserialize;
    handlers->deserialize_cleanup = ddw_deserialize_cleanup;

    handlers->unregister = ddw_unregister;

    ddw_serialize_register();

    serialization_addSerializer(handlers, SERIALIZE_FORMAT(serialize_format_ddw, 0, 0));

    SAH_TRACEZ_INFO("ddw", "loaded ddw serializer plugin");
}

