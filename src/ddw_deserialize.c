/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <debug/sahtrace.h>
#include <inttypes.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include "ddw.h"

#define containerof(item, type, member) (type*) (((char*) item) - offsetof(type, member))

typedef enum __ddw_state {
    ddw_state_init,
    ddw_state_reply_end,
    ddw_state_functionReturnValue,
    ddw_state_functionReturnArguments,
    ddw_state_request,
    ddw_state_request_parameter,
    ddw_state_objectBegin,
    ddw_state_objectBegin_function,
    ddw_state_objectBegin_function_argument,
    ddw_state_objectEnd,
    ddw_state_parameter,
    ddw_state_notification,
    ddw_state_error,
    ddw_state_count,
} ddw_state_t;

typedef struct _variant_stack_value {
    llist_iterator_t it;
    variant_t* value;
    variant_list_iterator_t* lit;
    variant_map_iterator_t* mit;
} variant_stack_value_t;

typedef struct __ddw {
    pcb_t* pcb;
    peer_info_t* peer;
    char* buflower;
    char* bufupper;
    char* bufstart;
    char* bufend;
    char* bufconverted;
    ddw_state_t state;
    request_t* request;
    uint32_t reqid;
    object_t* object;
    function_t* function;
    variant_t functionReturnValue;
    argument_value_list_t functionReturnArguments;
    llist_t variant_stack;
    bool stop;
} ddw_t;

static void ddw_variant_stack_push(ddw_t* ddw, variant_t* variant, variant_list_iterator_t* lit, variant_map_iterator_t* mit) {
    variant_stack_value_t* newvalue = (variant_stack_value_t*) calloc(1, sizeof(variant_stack_value_t));
    newvalue->value = variant;
    newvalue->lit = lit;
    newvalue->mit = mit;
    llist_prepend(&ddw->variant_stack, &newvalue->it);
}

static variant_stack_value_t* ddw_variant_stack_pop_element(ddw_t* ddw) {
    llist_iterator_t* it = llist_takeFirst(&ddw->variant_stack);
    variant_stack_value_t* sv = llist_item_data(it, variant_stack_value_t, it);
    return sv;
}

static void ddw_variant_stack_push_element(ddw_t* ddw, variant_stack_value_t* sv) {
    llist_prepend(&ddw->variant_stack, &sv->it);
}

variant_t* ddw_variant_stack_get(ddw_t* ddw) {
    llist_iterator_t* it = llist_first(&ddw->variant_stack);
    variant_stack_value_t* sv = llist_item_data(it, variant_stack_value_t, it);
    return sv->value;
}

static bool ddw_variant_stack_is_empty(ddw_t* ddw) {
    return llist_isEmpty(&ddw->variant_stack);
}

static void ddw_fdVariantsFetch(ddw_t* ddw, variant_t* var) {
    if(!peer_hasReceivedFd(ddw->peer)) {
        return;
    }
    SAH_TRACEZ_INFO("ddw_fd", "Search fd variants and set file descriptor");
    switch(variant_type(var)) {
    case variant_type_file_descriptor: {
        int fd = peer_fetchFd(ddw->peer);
        SAH_TRACEZ_INFO("ddw_fd", "Setting file descriptor %d to variant", fd);
        variant_setFd(var, fd);
    }
    break;
    case variant_type_array: {
        variant_list_iterator_t* it = NULL;
        variant_list_t* list = variant_da_list(var);
        variant_list_for_each(it, list) {
            ddw_fdVariantsFetch(ddw, variant_list_iterator_data(it));
        }
    }
    break;
    case variant_type_map: {
        variant_map_iterator_t* it = NULL;
        variant_map_t* list = variant_da_map(var);
        variant_map_for_each(it, list) {
            ddw_fdVariantsFetch(ddw, variant_map_iterator_data(it));
        }
    }
    break;
    default:
        // all other types nothing to do
        break;
    }
}

bool ddw_deserialize_checkFormat_V01(peer_info_t* peer) {
    char buf[4] = "";
    peer_peek(peer, buf, 4);
    SAH_TRACEZ_INFO("ddw", "In ddw_deserialize_checkFormat() buf=0x%02x%02x%02x%02x", buf[0], buf[1], buf[2], buf[3]);
    return !strncmp(buf, "DDW", 3) && buf[3] == DDW_VERSION_01;
}

static char ddw_deserialize_tag(char** buf, char* bufend, bool* running) {
    if(!*running) {
        return '\0';
    }
    if(bufend - *buf < 5) {
        *running = false;
        return '\0';
    }
    char retval = (*buf)[4];
    *buf += 5;
    return retval;
}

static uint32_t ddw_deserialize_uint32(char** buf, char* bufend, char* bufconv, bool* running) {
    if(!*running) {
        return 0;
    }
    if(bufend - *buf < 4) {
        *running = false;
        return 0;
    }
    if(*buf + 4 > bufconv) {
        ddw_byteswap(*buf, 4);
    }
    uint32_t retval;
    memcpy(&retval, *buf, 4);
    *buf += 4;
    return retval;
}

static uint64_t ddw_deserialize_uint64(char** buf, char* bufend, char* bufconv, bool* running) {
    if(!*running) {
        return 0;
    }
    if(bufend - *buf < 8) {
        *running = false;
        return 0;
    }
    if(*buf + 8 > bufconv) {
        ddw_byteswap(*buf, 8);
    }
    uint64_t retval;
    memcpy(&retval, *buf, 8);
    *buf += 8;
    return retval;
}

static int32_t ddw_deserialize_int32(char** buf, char* bufend, char* bufconv, bool* running) {
    if(!*running) {
        return 0;
    }
    if(bufend - *buf < 4) {
        *running = false;
        return 0;
    }
    if(*buf + 4 > bufconv) {
        ddw_byteswap(*buf, 4);
    }
    int32_t retval;
    memcpy(&retval, *buf, 4);
    *buf += 4;
    return retval;
}

static int64_t ddw_deserialize_int64(char** buf, char* bufend, char* bufconv, bool* running) {
    if(!*running) {
        return 0;
    }
    if(bufend - *buf < 8) {
        *running = false;
        return 0;
    }
    if(*buf + 8 > bufconv) {
        ddw_byteswap(*buf, 8);
    }
    int64_t retval;
    memcpy(&retval, *buf, 8);
    *buf += 8;
    return retval;
}

static char* ddw_deserialize_string(char** buf, char* bufend, bool* running) {
    if(!*running) {
        return NULL;
    }
    char* bufptr = *buf;
    while(bufptr < bufend && *bufptr) {
        bufptr++;
    }
    if(bufptr == bufend) {
        *running = false;
        return NULL;
    }
    char* retval = *buf;
    *buf = bufptr + 1;
    return retval;
}

static char* ddw_deserialize_byte_array(uint32_t* size, char** buf, char* bufend, char* bufconv, bool* running) {
    if(!*running) {
        return NULL;
    }
    *size = ddw_deserialize_int32(buf, bufend, bufconv, running);
    if(!(*running)) {
        *size = 0;
        return NULL;
    }
    char* bufptr = *buf;
    if((bufptr + *size) > bufend) {
        *running = false;
        return NULL;
    }
    *buf = bufptr + *size;

    return bufptr;
}

static double ddw_deserialize_double(char** buf, char* bufend, char* bufconv, bool* running) {
    if(!*running) {
        return 0;
    }
    if(bufend - *buf < (int32_t ) sizeof(double)) {
        *running = false;
        return 0;
    }
    if(*buf + sizeof(double) > bufconv) {
        ddw_byteswap(*buf, sizeof(double));
    }
    double retval;
    memcpy(&retval, *buf, sizeof(double));
    *buf += sizeof(double);
    return retval;
}

/*
 * @param v The state of the input variant MUST be initialized to type variant_type_unknown. Any other state can lead to a mem leak
 */
static uint32_t ddw_deserialize_variant_old(ddw_t* ddw, variant_t* v, char** buf, char* bufend, char* bufconv, bool* running) {
    if(!running || !*running) {
        return variant_type_unknown;
    }
    char* bufptr = *buf;
    uint32_t type = ddw_deserialize_uint32(&bufptr, bufend, bufconv, running);
    if(!*running) {
        return variant_type_unknown;
    }
    switch(type) {
    case variant_type_unknown:
        variant_setType(v, variant_type_unknown);
        break;
    case variant_type_string:
    case variant_type_date_time:
        variant_setChar(v, ddw_deserialize_string(&bufptr, bufend, running));
        break;
    case variant_type_int8:
        variant_setInt8(v, ddw_deserialize_int32(&bufptr, bufend, bufconv, running));
        break;
    case variant_type_int16:
        variant_setInt16(v, ddw_deserialize_int32(&bufptr, bufend, bufconv, running));
        break;
    case variant_type_int32:
        variant_setInt32(v, ddw_deserialize_int32(&bufptr, bufend, bufconv, running));
        break;
    case variant_type_int64:
        variant_setInt64(v, ddw_deserialize_int64(&bufptr, bufend, bufconv, running));
        break;
    case variant_type_bool:
        variant_setBool(v, ddw_deserialize_uint32(&bufptr, bufend, bufconv, running) ? true : false);
        break;
    case variant_type_uint8:
        variant_setUInt8(v, ddw_deserialize_uint32(&bufptr, bufend, bufconv, running));
        break;
    case variant_type_uint16:
        variant_setUInt16(v, ddw_deserialize_uint32(&bufptr, bufend, bufconv, running));
        break;
    case variant_type_uint32:
        variant_setUInt32(v, ddw_deserialize_uint32(&bufptr, bufend, bufconv, running));
        break;
    case variant_type_uint64:
        variant_setUInt64(v, ddw_deserialize_uint64(&bufptr, bufend, bufconv, running));
        break;
    case variant_type_double:
        variant_setDouble(v, ddw_deserialize_double(&bufptr, bufend, bufconv, running));
        break;
    case variant_type_array: {
        variant_initialize(v, variant_type_array);
        variant_list_t* list = variant_da_list(v);

        variant_t value;
        variant_initialize(&value, variant_type_unknown);
        while(ddw_deserialize_variant_old(ddw, &value, &bufptr, bufend, bufconv, running) != EOV && *running) {
            bool move_done = variant_list_addMove(list, &value);
            /* if it failed, the current variant has not been moved, thus it should be cleaned or it will be overwritten/leaked by the next call to ddw_deserialize_variant_old */
            if(!move_done) {
                variant_cleanup(&value);
                variant_initialize(&value, variant_type_unknown);
            }
        }
        variant_cleanup(&value);
        break;
    }
    case variant_type_map: {
        variant_initialize(v, variant_type_map);
        variant_map_t* map = variant_da_map(v);

        variant_t value;
        variant_initialize(&value, variant_type_unknown);
        while(ddw_deserialize_variant_old(ddw, &value, &bufptr, bufend, bufconv, running) != EOV && *running) {
            bool move_done = variant_map_addMove(map, ddw_deserialize_string(&bufptr, bufend, running), &value);
            /* if it failed, the current variant has not been moved, thus it should be cleaned or it will be overwritten/leaked by the next call to ddw_deserialize_variant_old */
            if(!move_done) {
                variant_cleanup(&value);
                variant_initialize(&value, variant_type_unknown);
            }
        }
        variant_cleanup(&value);
        break;
    }
    case variant_type_file_descriptor: {
        variant_setType(v, variant_type_file_descriptor);
        break;
    }
    case variant_type_byte_array: {
        uint32_t size = 0;
        void* data = ddw_deserialize_byte_array(&size, &bufptr, bufend, bufconv, running);
        variant_setByteArray(v, data, size);
    }
    break;
    default:
        break;
    }
    if(running) {
        *buf = bufptr;
        return type;
    } else {
        return variant_type_unknown;
    }
}

static uint32_t ddw_deserialize_variant(ddw_t* ddw, char** buf, char* bufend, char* bufconv, bool* running) {
    if(!*running) {
        return variant_type_unknown;
    }

    char* bufptr = *buf;
    variant_t* value = ddw_variant_stack_get(ddw);
    variant_t* v = NULL;

    variant_list_iterator_t* lit = NULL;
    variant_map_iterator_t* mit = NULL;

    // fetch variant type
    uint32_t type = ddw_deserialize_uint32(&bufptr, bufend, bufconv, running);
    if(!*running) {
        return variant_type_unknown;
    }

    if(type == EOV) {
        // pop from stack
        variant_stack_value_t* sv = ddw_variant_stack_pop_element(ddw);
        if(!ddw_variant_stack_is_empty(ddw)) {
            value = ddw_variant_stack_get(ddw);
            // need key
            if(variant_type(value) == variant_type_map) {
                char* string = ddw_deserialize_string(&bufptr, bufend, running);
                if(!*running) {
                    // key not available, re-add to stack
                    ddw_variant_stack_push_element(ddw, sv);
                    return variant_type_unknown;
                }
                variant_map_iterator_setKey(sv->mit, string);
            }
        }
        free(sv);
        *buf = bufptr;
        return type;
    }

    switch(variant_type(value)) {
    case variant_type_array: {
        // add variant to stack
        lit = variant_list_iterator_create(NULL);
        variant_list_append(variant_da_list(value), lit);
        v = variant_list_iterator_data(lit);
        ddw_variant_stack_push(ddw, v, lit, NULL);
    }
    break;
    case variant_type_map:
        // add variant to stack
        mit = variant_map_iterator_create("12345678901234567890123456789012", NULL);
        variant_map_append(variant_da_map(value), mit);
        v = variant_map_iterator_data(mit);
        ddw_variant_stack_push(ddw, v, NULL, mit);
        break;
    default:
        v = value;
        break;
    }

    switch(type) {
    case variant_type_unknown:
        variant_setType(v, variant_type_unknown);
        break;
    case variant_type_string:
    case variant_type_date_time:
        variant_setChar(v, ddw_deserialize_string(&bufptr, bufend, running));
        break;
    case variant_type_int8:
        variant_setInt8(v, ddw_deserialize_int32(&bufptr, bufend, bufconv, running));
        break;
    case variant_type_int16:
        variant_setInt16(v, ddw_deserialize_int32(&bufptr, bufend, bufconv, running));
        break;
    case variant_type_int32:
        variant_setInt32(v, ddw_deserialize_int32(&bufptr, bufend, bufconv, running));
        break;
    case variant_type_int64:
        variant_setInt64(v, ddw_deserialize_int64(&bufptr, bufend, bufconv, running));
        break;
    case variant_type_bool:
        variant_setBool(v, ddw_deserialize_uint32(&bufptr, bufend, bufconv, running) ? true : false);
        break;
    case variant_type_uint8:
        variant_setUInt8(v, ddw_deserialize_uint32(&bufptr, bufend, bufconv, running));
        break;
    case variant_type_uint16:
        variant_setUInt16(v, ddw_deserialize_uint32(&bufptr, bufend, bufconv, running));
        break;
    case variant_type_uint32:
        variant_setUInt32(v, ddw_deserialize_uint32(&bufptr, bufend, bufconv, running));
        break;
    case variant_type_uint64:
        variant_setUInt64(v, ddw_deserialize_uint64(&bufptr, bufend, bufconv, running));
        break;
    case variant_type_double:
        variant_setDouble(v, ddw_deserialize_double(&bufptr, bufend, bufconv, running));
        break;
    case variant_type_file_descriptor:
        variant_setType(v, variant_type_file_descriptor);
        break;
    case variant_type_byte_array: {
        uint32_t size = 0;
        void* data = ddw_deserialize_byte_array(&size, &bufptr, bufend, bufconv, running);
        variant_setByteArray(v, data, size);
    }
    break;
    case variant_type_array: {
        // initialize variant to list
        variant_initialize(v, variant_type_array);
    }
    break;
    case variant_type_map: {
        // initialize variant to list
        variant_initialize(v, variant_type_map);
    }
    break;
    default:
        SAH_TRACEZ_ERROR("ddw", "Invalid variant type %d (%p) buffer size = %zd", type, ddw, bufend - bufptr);
        break;
    }

    if(!*running) {
        return variant_type_unknown;
    }

    switch(type) {
    case variant_type_array:
    case variant_type_map:
        // do not pop from stack until EOV is reached
        break;
    default: {
        // pop from stack
        variant_stack_value_t* sv = ddw_variant_stack_pop_element(ddw);
        if(!ddw_variant_stack_is_empty(ddw)) {
            value = ddw_variant_stack_get(ddw);
            if(variant_type(value) == variant_type_map) {
                // need key
                char* string = ddw_deserialize_string(&bufptr, bufend, running);
                if(!*running) {
                    // key not available, re-add to stack
                    ddw_variant_stack_push_element(ddw, sv);
                    SAH_TRACEZ_OUT("ddw");
                    return variant_type_unknown;
                }
                variant_map_iterator_setKey(sv->mit, string);
            }
        }
        free(sv);
    }
    break;
    }

    *buf = bufptr;
    return type;
}

static bool ddw_deserialize_init(ddw_t* ddw, char** buf, char* bufend) {
    char* bufptr = *buf;
    bool running = true;
    char tag = ddw_deserialize_tag(&bufptr, bufend, &running);
    if(!running) {
        return false;
    }
    if(tag == 'D') {
        ddw->state = ddw_state_reply_end;
    } else if(tag == 'V') {
        variant_setType(&ddw->functionReturnValue, variant_type_unknown);
        ddw->state = ddw_state_functionReturnValue;
    } else if(tag == 'A') {
        argument_valueClear(&ddw->functionReturnArguments);
        ddw->state = ddw_state_functionReturnArguments;
    } else if(tag == 'R') {
        ddw->state = ddw_state_request;
    } else if(tag == 'O') {
        ddw->state = ddw_state_objectBegin;
    } else if(tag == 'Q') {
        ddw->state = ddw_state_objectEnd;
    } else if(tag == 'P') {
        ddw->state = ddw_state_parameter;
    } else if(tag == 'N') {
        ddw->state = ddw_state_notification;
    } else if(tag == 'E') {
        ddw->state = ddw_state_error;
    } else {
        SAH_TRACEZ_ERROR("ddw", "Unexpected tag '%c'(%p)", tag, ddw);
    }
    *buf = bufptr;
    return true;
}

static bool ddw_deserialize_reply_end(ddw_t* ddw, char** buf, char* bufend) {
    char* bufptr = *buf;
    bool running = true;
    uint32_t id = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
    if(!running) {
        return false;
    }
    SAH_TRACEZ_INFO("ddw", "Reply end for request 0x%x (peer = %d)", id, peer_getFd(ddw->peer));
    request_t* req = pcb_getPendingRequest(ddw->peer, id);
    reply_t* reply = request_reply(req);
    if(reply) {
        if(request_type(req) == request_type_exec_function) {
            reply_addFunctionReturn(reply, &ddw->functionReturnValue, &ddw->functionReturnArguments);
            variant_setType(&ddw->functionReturnValue, variant_type_unknown);
            argument_valueClear(&ddw->functionReturnArguments);
        }
        reply_setCompleted(reply, true);
    } else {
        SAH_TRACEZ_NOTICE("ddw", "Request not found 0x%x in peer %d", id, peer_getFd(ddw->peer));
    }
    ddw->state = ddw_state_init;
    *buf = bufptr;
    return true;
}

static bool ddw_deserialize_functionReturnValue(ddw_t* ddw, char** buf, char* bufend) {
    char* bufptr = *buf;
    bool running = true;

    if(llist_isEmpty(&ddw->variant_stack)) {
        // fetch request id
        ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
        if(running) {
            variant_initialize(&ddw->functionReturnValue, variant_type_unknown);
            ddw_variant_stack_push(ddw, &ddw->functionReturnValue, NULL, NULL);
        } else {
            return false;
        }
    }
    // fetch variant
    while(!llist_isEmpty(&ddw->variant_stack) && running) {
        ddw_deserialize_variant(ddw, &bufptr, bufend, ddw->bufconverted, &running);
        *buf = bufptr;
    }
    if(!running) {
        return false;
    }

    ddw_fdVariantsFetch(ddw, &ddw->functionReturnValue);
    ddw->state = ddw_state_init;
    return true;
}

static bool ddw_deserialize_functionReturnArguments(ddw_t* ddw, char** buf, char* bufend) {
    char* bufptr = *buf;
    bool running = true;
    static variant_t* value = NULL;

    if(llist_isEmpty(&ddw->variant_stack)) {
        // fetch request id
        ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
        if(running) {
            value = calloc(1, sizeof(variant_t));
            variant_initialize(value, variant_type_map);
            ddw_variant_stack_push(ddw, value, NULL, NULL);
            argument_valueClear(&ddw->functionReturnArguments);
        } else {
            return false;
        }
    }

    while(!llist_isEmpty(&ddw->variant_stack) && running) {
        ddw_deserialize_variant(ddw, &bufptr, bufend, ddw->bufconverted, &running);
        *buf = bufptr;
    }

    if(!running) {
        return false;
    }

    variant_map_iterator_t* it = NULL;
    variant_map_for_each(it, variant_da_map(value)) {
        const char* name = variant_map_iterator_key(it);
        variant_t* data = variant_map_iterator_data(it);
        SAH_TRACEZ_INFO("ddw", "Add out argument %s", name);
        argument_valueSet(&ddw->functionReturnArguments, name, data);
        ddw_fdVariantsFetch(ddw, data);
    }

    *buf = bufptr;
    variant_cleanup(value);
    free(value);
    value = NULL;

    ddw->state = ddw_state_init;
    *buf = bufptr;
    return true;
}

static bool ddw_deserialize_request(ddw_t* ddw, char** buf, char* bufend) {
    char* bufptr = *buf;
    bool running = true;

    uint32_t id = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
    uint32_t type = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);

    if(!running) {
        return false;
    }

    if(type == request_type_close_request) {
        uint32_t originalRequestId = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
        ddw_deserialize_tag(&bufptr, bufend, &running); // ignore tag - it must be the end of the request

        if(!running) {
            return false;
        }

        request_t* request = request_create_closeRequest(originalRequestId);
        serialization_setRequestId(request, id);
        pcb_addReceivedRequest(ddw->peer, request);
        ddw->stop = true;
        ddw->state = ddw_state_init;
    } else if(type == request_type_open_session) {
        char* username = ddw_deserialize_string(&bufptr, bufend, &running);
        ddw_deserialize_tag(&bufptr, bufend, &running); // ignore tag - it must be the end of the request

        if(!running) {
            return false;
        }

        request_t* request = request_create_openSession(username);
        serialization_setRequestId(request, id);
        pcb_addReceivedRequest(ddw->peer, request);
        ddw->stop = true;
        ddw->state = ddw_state_init;

    } else {
        char* path = ddw_deserialize_string(&bufptr, bufend, &running);
        char* pattern = ddw_deserialize_string(&bufptr, bufend, &running);
        char* function = ddw_deserialize_string(&bufptr, bufend, &running);
        ddw_deserialize_string(&bufptr, bufend, &running);                    // ignore retval, used for backwards compatibility
        ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running); // ignore retval, used for backwards compatibility
        uint32_t attributes = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
        uint32_t depth = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
        uint32_t index = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
        char* key = ddw_deserialize_string(&bufptr, bufend, &running);
        uint32_t userID = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
        uint32_t sourcePid = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
        char tag = ddw_deserialize_tag(&bufptr, bufend, &running);

        if(!running) {
            return false;
        }

        switch(type) {
        case request_type_get_object:      ddw->request = request_create_getObject(path, depth, attributes); break;
        case request_type_set_object:      ddw->request = request_create_setObject(path, attributes); break;
        case request_type_create_instance: ddw->request = request_create_addInstance(path, index, key, attributes); break;
        case request_type_delete_instance: ddw->request = request_create_deleteInstance(path, attributes); break;
        case request_type_find_objects:    ddw->request = request_create_findObjects(path, pattern, depth, attributes); break;
        case request_type_exec_function:   ddw->request = request_create_executeFunction(path, function, attributes); break;
        default: ddw->request = NULL; break;
        }
        if(ddw->request) {
            serialization_setRequestId(ddw->request, id);
            request_setUserID(ddw->request, userID);
            request_setPid(ddw->request, sourcePid);
        }

        if(tag == 'R') {
            if(ddw->request) {
                pcb_addReceivedRequest(ddw->peer, ddw->request);
                ddw->stop = true;
            }
            ddw->request = NULL;
            ddw->state = ddw_state_init;
        } else if(tag == 'P') {
            ddw->state = ddw_state_request_parameter;
        } else {
            SAH_TRACEZ_ERROR("ddw", "Unexpected tag '%c'", tag);
        }
    }

    *buf = bufptr;
    return true;
}

static bool ddw_deserialize_request_parameter(ddw_t* ddw, char** buf, char* bufend) {
    char* bufptr = *buf;
    bool running = true;

    char* name = ddw_deserialize_string(&bufptr, bufend, &running);
    variant_t value;
    variant_initialize(&value, variant_type_unknown);
    ddw_deserialize_variant_old(ddw, &value, &bufptr, bufend, ddw->bufconverted, &running);
    char tag = ddw_deserialize_tag(&bufptr, bufend, &running);

    if(!running) {
        variant_cleanup(&value);
        return false;
    }

    ddw_fdVariantsFetch(ddw, &value);
    if(ddw->request) {
        request_addParameter(ddw->request, name, &value);
    }
    variant_cleanup(&value);

    if(tag == 'R') {
        if(ddw->request) {
            pcb_addReceivedRequest(ddw->peer, ddw->request);
            ddw->stop = true;
        }
        ddw->request = NULL;
        ddw->state = ddw_state_init;
    }

    *buf = bufptr;
    return true;
}

static void ddw_destroy_object(object_t* object, void* userdata) {
    (void) object;
    ddw_t* ddw = (ddw_t*) userdata;
    SAH_TRACEZ_INFO("ddw", "In object destroyHandler for object %s", object_name(object, path_attr_key_notation));
    ddw->object = NULL;
    ddw->function = NULL;

}

static void ddw_push_object(ddw_t* ddw) {
    if(!ddw->object) {
        return;
    }
    request_t* req = pcb_getPendingRequest(ddw->peer, ddw->reqid);
    serialization_setObjectDestroyHandler(ddw->object, NULL, NULL);
    if(req) {
        SAH_TRACEZ_INFO("ddw", "Adding object %s to reply", object_name(ddw->object, 0));
        object_t* parent = pcb_getRootObject(ddw->pcb, req);
        serialization_pushObjectToCache(parent, ddw->object, req);
        reply_addObject(request_reply(req), ddw->object);
    } else if(object_name(ddw->object, 0) != NULL) {
        serialization_objectDelete(ddw->object);
    }
    ddw->object = NULL;
    ddw->function = NULL;
}

static bool ddw_deserialize_objectBegin(ddw_t* ddw, char** buf, char* bufend) {
    char* bufptr = *buf;
    bool running = true;

    ddw_push_object(ddw);

    uint32_t id = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
    char* indexpath = ddw_deserialize_string(&bufptr, bufend, &running);
    char* keypath = ddw_deserialize_string(&bufptr, bufend, &running);
    char* indexname = ddw_deserialize_string(&bufptr, bufend, &running);
    char* keyname = ddw_deserialize_string(&bufptr, bufend, &running);
    uint32_t attributes = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
    uint32_t parameterCount = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
    uint32_t childCount = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
    uint32_t instanceCount = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
    uint32_t functionCount = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
    uint32_t state = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
    uint32_t maxInstances = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
    char* description = ddw_deserialize_string(&bufptr, bufend, &running);

    char tag = ddw_deserialize_tag(&bufptr, bufend, &running);

    if(!running) {
        return false;
    }

    ddw->reqid = id;
    request_t* req = pcb_getPendingRequest(ddw->peer, id);
    if(!req) {
        SAH_TRACEZ_NOTICE("ddw", "request is not valid anymore, ignore replies");
    } else {
        object_t* parent = pcb_getRootObject(ddw->pcb, req);
        ddw->object = serialization_objectCreate(ddw->peer, parent,
                                                 indexpath, indexname, keypath, keyname, attributes, state, req);
        if(object_isTemplate(ddw->object)) {
            object_setMaxInstances(ddw->object, maxInstances);
        }
        if(!ddw->object) {
            SAH_TRACEZ_ERROR("ddw", "serialization_objectCreate failed");
        } else {
            serialization_objectSetParameterCount(ddw->object, parameterCount);
            serialization_objectSetChildCount(ddw->object, childCount);
            serialization_objectSetInstanceCount(ddw->object, instanceCount);
            serialization_objectSetFunctionCount(ddw->object, functionCount);
            serialization_setObjectDestroyHandler(ddw->object, ddw_destroy_object, ddw);
#ifdef PCB_HELP_SUPPORT
            object_setDescription(ddw->object, description);
#else
            (void) description;
#endif
        }
    }


    if(tag == 'F') {
        ddw->state = ddw_state_objectBegin_function;
    } else if(tag == 'O') {
        ddw->state = ddw_state_init;
    }

    *buf = bufptr;
    return true;
}

static bool ddw_deserialize_objectBegin_function(ddw_t* ddw, char** buf, char* bufend) {
    char* bufptr = *buf;
    bool running = true;

    char* name = ddw_deserialize_string(&bufptr, bufend, &running);
    uint32_t attributes = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
    uint32_t type = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
    char* typeName = ddw_deserialize_string(&bufptr, bufend, &running);
    char* description = ddw_deserialize_string(&bufptr, bufend, &running);
    char tag = ddw_deserialize_tag(&bufptr, bufend, &running);

    if(!running) {
        return false;
    }

    if(ddw->object) {
        ddw->function = serialization_functionCreate(ddw->object, name, type, typeName, attributes);
#ifdef PCB_HELP_SUPPORT
        function_setDescription(ddw->function, description);
#else
        (void) description;
#endif
    }

    if(tag == 'A') {
        ddw->state = ddw_state_objectBegin_function_argument;
    } else if(tag == 'F') {
        ddw->state = ddw_state_objectBegin_function;
    } else if(tag == 'O') {
        ddw->state = ddw_state_init;
    }

    *buf = bufptr;
    return true;
}

static bool ddw_deserialize_objectBegin_function_argument(ddw_t* ddw, char** buf, char* bufend) {
    char* bufptr = *buf;
    bool running = true;

    char* name = ddw_deserialize_string(&bufptr, bufend, &running);
    uint32_t attributes = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
    uint32_t type = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
    char* typeName = ddw_deserialize_string(&bufptr, bufend, &running);
    char tag = ddw_deserialize_tag(&bufptr, bufend, &running);

    if(!running) {
        return false;
    }

    if(ddw->function) {
        serialization_argumentCreate(ddw->function, name, type, typeName, attributes);
    }

    if(tag == 'A') {
        ddw->state = ddw_state_objectBegin_function_argument;
    } else if(tag == 'F') {
        ddw->state = ddw_state_objectBegin_function;
    } else if(tag == 'O') {
        ddw->state = ddw_state_init;
    }

    *buf = bufptr;
    return true;
}

static bool ddw_deserialize_parameter(ddw_t* ddw, char** buf, char* bufend) {
    char* bufptr = *buf;
    bool running = true;

    ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
    char* name = ddw_deserialize_string(&bufptr, bufend, &running);
    uint32_t attributes = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
    uint32_t type = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
    uint32_t state = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
    char* description = ddw_deserialize_string(&bufptr, bufend, &running);
    variant_t value;
    variant_initialize(&value, variant_type_unknown);
    ddw_deserialize_variant_old(ddw, &value, &bufptr, bufend, ddw->bufconverted, &running);

    char validator_tag = ddw_deserialize_tag(&bufptr, bufend, &running);
    variant_t validator_minimum;
    variant_t validator_maximum;
    variant_initialize(&validator_minimum, variant_type_unknown);
    variant_initialize(&validator_maximum, variant_type_unknown);
    variant_list_t* validator_values = NULL;
    switch(validator_tag) {
    case 'E': {
        validator_values = calloc(1, sizeof(variant_list_t));
        variant_list_initialize(validator_values);
        variant_t validator_value;
        variant_initialize(&validator_value, variant_type_unknown);
        while(ddw_deserialize_variant_old(ddw, &validator_value, &bufptr, bufend, ddw->bufconverted, &running) != EOV && running) {
            variant_list_add(validator_values, &validator_value);
            variant_cleanup(&validator_value);
            variant_initialize(&validator_value, variant_type_unknown);
        }
        variant_cleanup(&validator_value);
        break;
    }
    case 'R':
        ddw_deserialize_variant_old(ddw, &validator_minimum, &bufptr, bufend, ddw->bufconverted, &running);
        ddw_deserialize_variant_old(ddw, &validator_maximum, &bufptr, bufend, ddw->bufconverted, &running);
        break;
    case 'I':
        ddw_deserialize_variant_old(ddw, &validator_minimum, &bufptr, bufend, ddw->bufconverted, &running);
        break;
    case 'A':
        ddw_deserialize_variant_old(ddw, &validator_maximum, &bufptr, bufend, ddw->bufconverted, &running);
        break;
    case 'N':
    default:
        break;
    }

    if(!running) {
        goto leave;
    }

    if(ddw->object) {
        // XXX: object should be already reset by now
        SAH_TRACEZ_INFO("ddw", "addParameter object=%s name=%s", object_name(ddw->object, path_attr_key_notation), name);
        parameter_t* parameter = serialization_parameterCreate(ddw->object, name, type, attributes, state);
#ifdef PCB_HELP_SUPPORT
        parameter_setDescription(parameter, description);
#else
        (void) description;
#endif
        serialization_parameterSetValue(parameter, &value);

        switch(validator_tag) {
        case 'E':
            parameter_setValidator(parameter, param_validator_create_enum(validator_values));
            validator_values = NULL;
            break;
        case 'R':
            parameter_setValidator(parameter, param_validator_create_range(&validator_minimum, &validator_maximum));
            break;
        case 'I':
            parameter_setValidator(parameter, param_validator_create_minimum(&validator_minimum));
            break;
        case 'A':
            parameter_setValidator(parameter, param_validator_create_maximum(&validator_maximum));
            break;
        case 'N':
        default:
            break;
        }
    }
    ddw->state = ddw_state_init;
    *buf = bufptr;
leave:
    variant_cleanup(&value);
    variant_cleanup(&validator_minimum);
    variant_cleanup(&validator_maximum);
    if(validator_values) {
        variant_list_cleanup(validator_values);
        free(validator_values);
    }
    return running;
}

static bool ddw_deserialize_objectEnd(ddw_t* ddw, char** buf, char* bufend) {
    char* bufptr = *buf;
    bool running = true;

    ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);

    if(!running) {
        return false;
    }

    ddw_push_object(ddw);
    ddw->state = ddw_state_init;

    *buf = bufptr;
    return true;
}

static bool ddw_deserialize_notification(ddw_t* ddw, char** buf, char* bufend) {
    char* bufptr = *buf;
    bool running = true;

    uint32_t id = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
    uint32_t type = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
    char* name = ddw_deserialize_string(&bufptr, bufend, &running);
    char* objectPath = ddw_deserialize_string(&bufptr, bufend, &running);

    variant_map_t map;
    variant_map_initialize(&map);
    variant_t value;
    variant_initialize(&value, variant_type_unknown);
    while(ddw_deserialize_variant_old(ddw, &value, &bufptr, bufend, ddw->bufconverted, &running) != EOV && running) {
        variant_map_add(&map, ddw_deserialize_string(&bufptr, bufend, &running), &value);
        variant_cleanup(&value);
        variant_initialize(&value, variant_type_unknown);
    }
    variant_cleanup(&value);

    if(!running) {
        variant_map_cleanup(&map);
        return false;
    }

    variant_map_iterator_t* it = NULL;
    variant_map_for_each(it, &map) {
        ddw_fdVariantsFetch(ddw, variant_map_iterator_data(it));
    }
    request_t* req = pcb_getPendingRequest(ddw->peer, id);
    if(req) {
        SAH_TRACEZ_INFO("ddw_des", "Deserialized notification");
        SAH_TRACEZ_INFO("ddw_des", "    Notification type - %d", type);
        notification_t* notification = notification_create(type);
        notification_setName(notification, name);
        SAH_TRACEZ_INFO("ddw_des", "    Notification name - %s", name);
        if(*objectPath) {
            notification_setObjectPath(notification, objectPath);
            SAH_TRACEZ_INFO("ddw_des", "    Object path       - %s", objectPath);
        }
        variant_map_iterator_t* mit;
        variant_map_for_each(mit, &map) {
            notification_addParameter(notification, notification_parameter_createVariant(variant_map_iterator_key(mit), variant_map_iterator_data(mit)));
        }
        object_t* parent = pcb_getRootObject(ddw->pcb, req);
        serialization_updateCache(parent, notification);
        if(!reply_addNotification(request_reply(req), notification)) {
            SAH_TRACEZ_ERROR("ddw_des", "Failed to add notification to reply of request 0x%x", request_id(req));
        } else {
            SAH_TRACEZ_NOTICE("ddw_des", "Added notification to reply of request 0x%x", request_id(req));
        }
    } else {
        SAH_TRACEZ_NOTICE("ddw", "request is not valid anymore, ignore replies");
        SAH_TRACEZ_NOTICE("ddw_des", "Dropping notification");
    }

    ddw->state = ddw_state_init;
    variant_map_cleanup(&map);
    *buf = bufptr;
    return true;
}

bool ddw_deserialize_error(ddw_t* ddw, char** buf, char* bufend) {
    char* bufptr = *buf;
    bool running = true;

    uint32_t id = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
    uint32_t error = ddw_deserialize_uint32(&bufptr, bufend, ddw->bufconverted, &running);
    char* description = ddw_deserialize_string(&bufptr, bufend, &running);
    char* info = ddw_deserialize_string(&bufptr, bufend, &running);

    if(!running) {
        return false;
    }

    request_t* req = pcb_getPendingRequest(ddw->peer, id);
    if(req) {
        reply_addError(request_reply(req), error, description, info);
    }

    ddw->state = ddw_state_init;
    *buf = bufptr;
    return true;
}

static bool (* ddw_deserialize_step[ddw_state_count])(ddw_t* ddw, char** buf, char* bufend) = {
    ddw_deserialize_init,
    ddw_deserialize_reply_end,
    ddw_deserialize_functionReturnValue,
    ddw_deserialize_functionReturnArguments,
    ddw_deserialize_request,
    ddw_deserialize_request_parameter,
    ddw_deserialize_objectBegin,
    ddw_deserialize_objectBegin_function,
    ddw_deserialize_objectBegin_function_argument,
    ddw_deserialize_objectEnd,
    ddw_deserialize_parameter,
    ddw_deserialize_notification,
    ddw_deserialize_error,
};

static size_t ddw_realloc_buffer(ddw_t* ddw) {
    size_t bufsize;
    if (ddw == NULL) {
        SAH_TRACEZ_ERROR("ddw", "ddw pointer is NULL");
        return 0;
    }

    if((ddw->bufupper - ddw->bufend) < 512) {
        bufsize = ddw->bufupper - ddw->buflower + 1024;
    } else if((ddw->bufupper - ddw->bufend) < (1024 + 512)) {
        bufsize = ddw->bufupper - ddw->buflower;
    } else {
        bufsize = ddw->bufupper - ddw->buflower - 1024;
    }
    if (bufsize != (size_t)(ddw->bufupper - ddw->buflower)) {
        size_t startoffset = ddw->bufstart - ddw->buflower;
        size_t endoffset = ddw->bufend - ddw->buflower;
        size_t convoffset = ddw->bufconverted - ddw->buflower;

        ddw->buflower = realloc(ddw->buflower, bufsize);
        ddw->bufend = ddw->buflower;
        if(!ddw->buflower) {
            ddw->bufupper = NULL;
            SAH_TRACEZ_ERROR("ddw", "realloc(%lu) failed", (long unsigned)bufsize);
            return 0;
        }
        ddw->bufupper = ddw->buflower + bufsize;
        ddw->bufstart = ddw->buflower + startoffset;
        ddw->bufend = ddw->buflower + endoffset;
        ddw->bufconverted = ddw->buflower + convoffset;
    }

    return bufsize;
}

deserialize_return_t ddw_deserialize(peer_info_t* peer, pcb_t* connection) {
    if (peer == NULL || connection == NULL) {
        SAH_TRACEZ_ERROR("ddw", "peer or connection pointer is NULL");
        return deserialize_error;
    }

    bool atleast_one_fread_ok = false;
    ddw_t* ddw = NULL;
    ddw = (ddw_t*) pcb_getDeserializeInfo(peer);
    if(ddw == NULL) {
        ddw = (ddw_t*) calloc(1, sizeof(ddw_t));
        if(!ddw) {
            SAH_TRACEZ_ERROR("ddw", "calloc failed");
            return deserialize_error;
        }
        ddw->pcb = connection;
        ddw->peer = peer;
        variant_initialize(&ddw->functionReturnValue, variant_type_unknown);
        llist_initialize(&ddw->functionReturnArguments);
        pcb_setDeserializeInfo(peer, ddw);
    }

    ddw_realloc_buffer(ddw);
    ddw->stop = false;

    FILE* in = peer_inStream(peer);

    do {
        if((ddw->bufupper - ddw->bufend) < 512) {
            ddw_realloc_buffer(ddw);
        }
        ssize_t len = ddw->bufupper - ddw->bufend;
        if (!in) {
            SAH_TRACEZ_ERROR("ddw", "in pointer is NULL");
            break;
        }

        int n = fread(ddw->bufend, sizeof(char), len, in);
        if(n > 0) {
            atleast_one_fread_ok = true;
            ddw->bufend += n;
        }
        //an error occured, but we only quit if there was no previous read data
        else if(!atleast_one_fread_ok) {
            break;
        }

        if(n == len) {
            // we may still have data to read
            continue;
        }

        while(ddw_deserialize_step[ddw->state](ddw, &ddw->bufstart, ddw->bufend)) {
        }

        if(ddw->bufstart > ddw->buflower) {
            if(ddw->bufstart < ddw->bufend) {
                memmove(ddw->buflower, ddw->bufstart, ddw->bufend - ddw->bufstart);
            }
            ddw->bufend -= ddw->bufstart - ddw->buflower;
            ddw->bufstart = ddw->buflower;
        }

        ddw->bufconverted = ddw->bufend;

        if(ddw->stop) {
            errno = 0;
            break;
        }
        atleast_one_fread_ok = false;
    } while(true);

    if(ddw->stop) {
        return deserialize_more_available;
    }

    if(feof(in)) {
        return deserialize_error;
    } else if(errno && (errno != EAGAIN) && (errno != EINTR)) {
        return deserialize_error;
    } else if((errno == EINTR) || (errno == EAGAIN) || (ddw->bufend == ddw->bufupper)) {
        return deserialize_more_data;
    }
    return deserialize_done;
}

void ddw_deserialize_cleanup(peer_info_t* peer) {
    if(!peer) {
        SAH_TRACEZ_ERROR("ddw", "peer NULL pointer");
        return;
    }
    ddw_t* ddw = (ddw_t*) pcb_getDeserializeInfo(peer);
    if(ddw) {
        variant_cleanup(&ddw->functionReturnValue);
        llist_cleanup(&ddw->functionReturnArguments);
        free(ddw->buflower);
        if(ddw->object) {
            serialization_objectDelete(ddw->object);
        }
        free(ddw);
        pcb_setDeserializeInfo(peer, NULL);
    } else {
        SAH_TRACEZ_ERROR("ddw", "Failed to get deserialize Info from peer ");
    }
}

