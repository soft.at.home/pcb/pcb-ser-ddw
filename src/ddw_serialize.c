/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include "ddw.h"

#ifndef NDEBUG
static FILE* debugsink = NULL;
#endif

static inline void ddw_serialize_string(peer_info_t* peer, const char* s) {
    peer_fwrite(peer, s ? s : "", (s ? strlen(s) : 0) + 1, 1);
#ifndef NDEBUG
    if(debugsink) {
        fwrite(s ? s : "", (s ? strlen(s) : 0) + 1, 1, debugsink);
    }
#endif
}

static inline void ddw_serialize_byte_array(peer_info_t* peer, const void* d, uint32_t size) {
    ddw_byteswap((char*) &size, 4);
    peer_fwrite(peer, &size, 1, 4);
#ifndef NDEBUG
    if(debugsink) {
        fwrite(&size, 1, 4, debugsink);
    }
#endif
    ddw_byteswap((char*) &size, 4);
    peer_fwrite(peer, d, 1, size);
#ifndef NDEBUG
    if(debugsink) {
        fwrite(d, 1, size, debugsink);
    }
#endif
}

static inline void ddw_serialize_uint32(peer_info_t* peer, uint32_t u) {
    ddw_byteswap((char*) &u, 4);
    peer_fwrite(peer, &u, 1, 4);
#ifndef NDEBUG
    if(debugsink) {
        fwrite(&u, 1, 4, debugsink);
    }
#endif
}

static inline void ddw_serialize_uint64(peer_info_t* peer, uint64_t u) {
    ddw_byteswap((char*) &u, 8);
    peer_fwrite(peer, &u, 1, 8);
#ifndef NDEBUG
    if(debugsink) {
        fwrite(&u, 1, 8, debugsink);
    }
#endif
}

static inline void ddw_serialize_int32(peer_info_t* peer, int32_t i) {
    ddw_byteswap((char*) &i, 4);
    peer_fwrite(peer, &i, 1, 4);
#ifndef NDEBUG
    if(debugsink) {
        fwrite(&i, 1, 4, debugsink);
    }
#endif
}

static inline void ddw_serialize_int64(peer_info_t* peer, int64_t i) {
    ddw_byteswap((char*) &i, 8);
    peer_fwrite(peer, &i, 1, 8);
#ifndef NDEBUG
    if(debugsink) {
        fwrite(&i, 1, 8, debugsink);
    }
#endif
}

static inline void ddw_serialize_double(peer_info_t* peer, double d) {
    ddw_byteswap((char*) &d, sizeof(double));
    peer_fwrite(peer, &d, 1, sizeof(double));
#ifndef NDEBUG
    if(debugsink) {
        fwrite(&d, 1, sizeof(double), debugsink);
    }
#endif
}

static inline void ddw_serialize_tag(peer_info_t* peer, char tag) {
    static char tagbuf[5] = { 'D', 'D', 'W', DDW_VERSION_01, '\0' };
    tagbuf[4] = tag;
    peer_fwrite(peer, tagbuf, 5, 1);
#ifndef NDEBUG
    if(debugsink) {
        fwrite(tagbuf, 5, 1, debugsink);
    }
#endif
}

static void ddw_serialize_variant(peer_info_t* peer, const variant_t* v) {
    ddw_serialize_uint32(peer, variant_type(v));
    switch(variant_type(v)) {
    case variant_type_unknown:
        break;
    case variant_type_string:
        ddw_serialize_string(peer, string_buffer(variant_da_string(v)));
        break;
    case variant_type_date_time: {
        char* s = variant_char(v);
        ddw_serialize_string(peer, s);
        free(s);
        break;
    }
    case variant_type_int8:
    case variant_type_int16:
    case variant_type_int32:
        ddw_serialize_int32(peer, variant_int32(v));
        break;
    case variant_type_int64:
        ddw_serialize_int64(peer, variant_int64(v));
        break;
    case variant_type_bool:
    case variant_type_uint8:
    case variant_type_uint16:
    case variant_type_uint32:
        ddw_serialize_uint32(peer, variant_uint32(v));
        break;
    case variant_type_uint64:
        ddw_serialize_uint64(peer, variant_uint64(v));
        break;
    case variant_type_double:
        ddw_serialize_double(peer, variant_double(v));
        break;
    case variant_type_array: {
        variant_list_t* list = variant_da_list(v);
        variant_list_iterator_t* it = NULL;
        variant_list_for_each(it, list) {
            ddw_serialize_variant(peer, variant_list_iterator_data(it));
        }
        ddw_serialize_uint32(peer, EOV);
        break;
    }
    case variant_type_map: {
        variant_map_t* map = variant_da_map(v);
        variant_map_iterator_t* it = NULL;
        variant_map_for_each(it, map) {
            ddw_serialize_variant(peer, variant_map_iterator_data(it));
            ddw_serialize_string(peer, variant_map_iterator_key(it));
        }
        ddw_serialize_uint32(peer, EOV);
        break;
    }
    case variant_type_file_descriptor: {
        int fd = variant_fd(v);
        SAH_TRACEZ_INFO("ddw_fd", "Add file descriptor %d to socket", fd);
        peer_attachFd(peer, fd);
    }
    break;
    case variant_type_byte_array: {
        uint32_t size = 0;
        const void* data = variant_da_byteArray(v, &size);
        ddw_serialize_byte_array(peer, data, size);
    }
    break;
    default:
        break;
    }
}

bool ddw_serialize_reply_end(peer_info_t* peer, request_t* req) {
    ddw_serialize_tag(peer, 'D');
    ddw_serialize_uint32(peer, request_id(req));
    return true;
}

bool ddw_serialize_functionReturnValue(peer_info_t* peer, request_t* req, const variant_t* retval) {
    ddw_serialize_tag(peer, 'V');
    ddw_serialize_uint32(peer, request_id(req));
    ddw_serialize_variant(peer, retval);
    return true;
}

bool ddw_serialize_functionReturnArguments(peer_info_t* peer, request_t* req, argument_value_list_t* args) {
    ddw_serialize_tag(peer, 'A');
    ddw_serialize_uint32(peer, request_id(req));
    argument_value_t* arg;
    for(arg = argument_valueFirstArgument(args); arg; arg = argument_valueNextArgument(arg)) {
        ddw_serialize_variant(peer, argument_value(arg));
        ddw_serialize_string(peer, argument_valueName(arg));
    }
    ddw_serialize_uint32(peer, EOV);
    return true;
}

bool ddw_serialize_request(peer_info_t* peer, request_t* req) {
    ddw_serialize_tag(peer, 'R');
    ddw_serialize_uint32(peer, request_id(req));
    ddw_serialize_uint32(peer, request_type(req));
    if(request_type(req) == request_type_close_request) {
        ddw_serialize_uint32(peer, request_closeRequestId(req));
    } else if(request_type(req) == request_type_open_session) {
        ddw_serialize_string(peer, request_username(req));
    } else {
        ddw_serialize_string(peer, request_path(req));
        ddw_serialize_string(peer, request_pattern(req));
        ddw_serialize_string(peer, request_functionName(req));
        ddw_serialize_string(peer, "");
        ddw_serialize_uint32(peer, -1);
        ddw_serialize_uint32(peer, request_attributes(req));
        ddw_serialize_uint32(peer, request_depth(req));
        ddw_serialize_uint32(peer, request_instanceIndex(req));
        ddw_serialize_string(peer, request_instanceKey(req));
        ddw_serialize_uint32(peer, request_userID(req));
        ddw_serialize_uint32(peer, request_getPid(req));
        parameter_iterator_t* pit;
        for(pit = request_firstParameter(req); pit; pit = request_nextParameter(pit)) {
            ddw_serialize_tag(peer, 'P');
            ddw_serialize_string(peer, request_parameterName(pit));
            ddw_serialize_variant(peer, request_parameterValue(pit));
        }
    }
    ddw_serialize_tag(peer, 'R');
    return true;
}

bool ddw_serialize_objectBegin(peer_info_t* peer, request_t* req, object_t* object) {
    ddw_serialize_tag(peer, 'O');

    uint32_t attributes = path_attr_parent;
    if(request_attributes(req) & request_common_include_namespace) {
        attributes |= path_attr_include_namespace;
    }
    if(request_attributes(req) & request_common_path_alias) {
        attributes |= path_attr_alias;
    }
    string_t indexpath, keypath;
    string_initialize(&indexpath, 0);
    string_initialize(&keypath, 0);
    object_path(object, &indexpath, attributes);
    object_path(object, &keypath, attributes | path_attr_key_notation);

    ddw_serialize_uint32(peer, request_id(req));
    ddw_serialize_string(peer, string_buffer(&indexpath));
    ddw_serialize_string(peer, string_buffer(&keypath));

    const char* alias = object_da_parameterCharValue(object, "Alias");
    if(alias && *alias && object_isInstance(object) && !object_isRemote(object) && (attributes & path_attr_alias)) {
        string_t path;
        string_initialize(&path, 64);
        string_appendChar(&path, "[");
        string_appendChar(&path, object_name(object, attributes));
        string_appendChar(&path, "]");
        ddw_serialize_string(peer, string_buffer(&path));
        string_cleanup(&path);
    } else {
        ddw_serialize_string(peer, object_name(object, attributes));
    }

    ddw_serialize_string(peer, object_name(object, path_attr_key_notation | attributes));
    ddw_serialize_uint32(peer, object_attributes(object));
    ddw_serialize_uint32(peer, object_parameterCount(object));
    ddw_serialize_uint32(peer, object_childCount(object));
    ddw_serialize_uint32(peer, object_instanceCount(object));
    ddw_serialize_uint32(peer, object_functionCount(object));
    ddw_serialize_uint32(peer, object_state(object));
    ddw_serialize_uint32(peer, object_getMaxInstances(object));
#ifdef PCB_HELP_SUPPORT
    if(request_attributes(req) & request_getObject_description) {
        ddw_serialize_string(peer, object_getDescription(object));
    } else {
        ddw_serialize_string(peer, "");
    }
#else
    ddw_serialize_string(peer, "");
#endif

    string_cleanup(&indexpath);
    string_cleanup(&keypath);

    if((request_attributes(req) & request_getObject_functions)
       && ((object_isTemplate(object) && (request_attributes(req) & request_getObject_template_info))
           || !object_isTemplate(object))) {
        function_t* function = NULL;
        object_for_each_function(function, object) {
            if(function_canRead(function, request_userID(req)) && function_canExecute(function, request_userID(req))) {
                ddw_serialize_tag(peer, 'F');
                ddw_serialize_string(peer, function_name(function));
                ddw_serialize_uint32(peer, function_attributes(function));
                ddw_serialize_uint32(peer, function_type(function));
                ddw_serialize_string(peer, function_typeName(function));
#ifdef PCB_HELP_SUPPORT
                if(request_attributes(req) & request_getObject_description) {
                    ddw_serialize_string(peer, function_getDescription(function));
                } else {
                    ddw_serialize_string(peer, "");
                }
#else
                ddw_serialize_string(peer, "");
#endif
                function_argument_t* arg;
                for(arg = function_firstArgument(function); arg; arg = function_nextArgument(arg)) {
                    ddw_serialize_tag(peer, 'A');
                    ddw_serialize_string(peer, argument_name(arg));
                    ddw_serialize_uint32(peer, argument_attributes(arg));
                    ddw_serialize_uint32(peer, argument_type(arg));
                    ddw_serialize_string(peer, argument_typeName(arg));
                }
            }
        }
    }
    ddw_serialize_tag(peer, 'O');
    return true;
}

bool ddw_serialize_objectEnd(peer_info_t* peer, request_t* req) {
    ddw_serialize_tag(peer, 'Q');
    ddw_serialize_uint32(peer, request_id(req));
    return true;
}

bool ddw_serialize_parameter(peer_info_t* peer, request_t* req, parameter_t* parameter, bool hideValue) {
    ddw_serialize_tag(peer, 'P');
    ddw_serialize_uint32(peer, request_id(req));
    ddw_serialize_string(peer, parameter_name(parameter));
    ddw_serialize_uint32(peer, parameter_attributes(parameter));
    ddw_serialize_uint32(peer, parameter_type(parameter));
    ddw_serialize_uint32(peer, parameter_state(parameter));
#ifdef PCB_HELP_SUPPORT
    if(request_attributes(req) & request_getObject_description) {
        ddw_serialize_string(peer, parameter_getDescription(parameter));
    } else {
        ddw_serialize_string(peer, "");
    }
#else
    ddw_serialize_string(peer, "");
#endif
    if(hideValue) {
        variant_t dummy;
        variant_initialize(&dummy, variant_type_unknown);
        ddw_serialize_variant(peer, &dummy);
        variant_cleanup(&dummy);
    } else {
        ddw_serialize_variant(peer, parameter_getValue(parameter));
    }
    if(request_attributes(req) & request_getObject_validators) {
        parameter_validator_t* validator = parameter_getValidator(parameter);
        if(validator) {
            switch(param_validator_type(validator)) {
            case parameter_validator_enum: {
                ddw_serialize_tag(peer, 'E');
                variant_list_iterator_t* it;
                variant_list_for_each(it, param_validator_values(validator)) {
                    ddw_serialize_variant(peer, variant_list_iterator_data(it));
                }
                ddw_serialize_uint32(peer, EOV);
                break;
            }
            case parameter_validator_range: {
                ddw_serialize_tag(peer, 'R');
                ddw_serialize_variant(peer, param_validator_minimum(validator));
                ddw_serialize_variant(peer, param_validator_maximum(validator));
                break;
            }
            case parameter_validator_minimum: {
                ddw_serialize_tag(peer, 'I');
                ddw_serialize_variant(peer, param_validator_minimum(validator));
                break;
            }
            case parameter_validator_maximum: {
                ddw_serialize_tag(peer, 'A');
                ddw_serialize_variant(peer, param_validator_maximum(validator));
                break;
            }
            default:
                ddw_serialize_tag(peer, 'N');
                break;
            }
        } else {
            ddw_serialize_tag(peer, 'N');
        }
    } else {
        ddw_serialize_tag(peer, 'N');
    }
    return true;
}

bool ddw_serialize_notification(peer_info_t* peer, request_t* req, notification_t* notification) {
    ddw_serialize_tag(peer, 'N');
    ddw_serialize_uint32(peer, request_id(req));
    ddw_serialize_uint32(peer, notification_type(notification));
    ddw_serialize_string(peer, notification_name(notification));
    ddw_serialize_string(peer, notification_objectPath(notification));
    notification_parameter_t* parameter;
    for(parameter = notification_firstParameter(notification); parameter; parameter = notification_nextParameter(parameter)) {
        ddw_serialize_variant(peer, notification_parameter_variant(parameter));
        ddw_serialize_string(peer, notification_parameter_name(parameter));
    }
    ddw_serialize_uint32(peer, EOV);
    return true;
}

bool ddw_serialize_error(peer_info_t* peer, request_t* req, uint32_t error, const char* description, const char* info) {
    ddw_serialize_tag(peer, 'E');
    ddw_serialize_uint32(peer, request_id(req));
    ddw_serialize_uint32(peer, error);
    ddw_serialize_string(peer, description);
    ddw_serialize_string(peer, info);
    return true;
}

void ddw_serialize_register() {
#ifndef NDEBUG
    char* debugsinkname = getenv("DDW_DEBUGSINK");
    if(debugsinkname && *debugsinkname) {
        debugsink = fopen(debugsinkname, "w");
    }
#endif
}

void ddw_serialize_unregister() {
#ifndef NDEBUG
    if(debugsink) {
        fclose(debugsink);
    }
#endif
}

