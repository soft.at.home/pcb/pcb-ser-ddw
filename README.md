# pcb-ser-ddw

## Summary

This library provides the code to serialize and deserialize
the requests going over a PCB connection into the DDW format.

## Description

A PCB plug-in creates requests in memory. When passed over
the PCB bus, these requests first need to be serialized to
a commonly known format, independent of word size or endianess.
The receiver of the request need to deserialize it first, before
it can process the request and send a reply.

The DDW serializer is used as main serializer for communication
between PCB plug-in's and the PCB system bus.
